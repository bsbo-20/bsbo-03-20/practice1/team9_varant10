﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uml
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CashRegister cashRegister;
            string gameName;


            cashRegister = new CashRegister();
            gameName = "Спартак-Зенит";
            cashRegister.getInf(gameName);


            cashRegister.createSale();
            Console.WriteLine('\n');

            for (int i = 0; i < 2; i++)
            {
                cashRegister.addTicket(i + 1);
                Console.WriteLine('\n');
            }

            cashRegister.getPriceSale();
            cashRegister.getListTicketSale();

            cashRegister.createTransaktion();
            cashRegister.pay(1000);


            Console.WriteLine("______________________________________________");

            string check = "12N382WER3";
            int idTicket = 1;

            cashRegister.createReturnd();
            cashRegister.checkReturnd(check, gameName);
            cashRegister.rsturmMoany(idTicket, check);




            Console.ReadLine();
        }

    }

        internal class CashRegister
        {
            Game[] game = new Game[2];
            Game gameForClient;
            Sale sale;
            Payment payment;
            returnd returnd;
            int mainPrice;
            List<int> ticket;
            bool resultReturnd;

            public CashRegister()
            {
                Console.WriteLine("Созан класс CashRegister");
                game[0] = new Game("Спартак-Зенит", 10);
                game[1] = new Game("Динамо-Зенит", 5);
            }

            public void getInf(string gameName)
            {
                Console.WriteLine("вызов метода getInf(gameName)");
                this.gameForClient = findGame(gameName);
                if (gameForClient == null)
                    Console.WriteLine("Game not find");

                string information = gameForClient.getInformation();
                Console.WriteLine("return Information");
                Console.WriteLine("\n\n" + information + "\n\n");
            }

            public Game getGame()
            {
                Console.WriteLine("вызов метода getGame()");
                Console.WriteLine("return Game");
                return this.gameForClient;
            }

            public Game findGame(string gameName)
            {
                Console.WriteLine("вызов метода findGame(nameGame)");
                for (int i = 0; i < game.Length; i++)
                    if (game[i].getName() == gameName)
                        return game[i];

                return null;
            }


            public void createSale()
            {
                Console.WriteLine("вызов метода createSale(game)");
                Console.WriteLine("Созан класс Sale");
                this.sale = new Sale(getGame());
            }

            public void addTicket(int place)
            {
                Console.WriteLine("вызов метода addTicket(place)");
                this.sale.addTicketToSale(place);
            }

            public void getPriceSale()
            {
                Console.WriteLine("вызов метода getPriceSale()");
                this.mainPrice = this.sale.getPrice();
                Console.WriteLine("return price");
                Console.WriteLine("Price: " + this.mainPrice);
            }

            public void getListTicketSale()
            {
                Console.WriteLine("вызов метода getListTicketSale()");
                this.ticket = this.sale.getListTicket();
                Console.WriteLine("return ListTicket");

                Console.Write("Ticket: ");
                foreach (int init in this.ticket)
                    Console.Write(init + " ");

                Console.WriteLine('\n');
            }

            public void createTransaktion()
            {
                Console.WriteLine("вызов метода createTransaktion(price, listTicket)");
                this.payment = new Payment(this.mainPrice, this.ticket);
                Console.WriteLine("Созан класс Payment");
            }

            public void pay(int money)
            {
                Console.WriteLine("вызов метода  pay(money)");
                string check = this.payment.pay(money);
                Console.WriteLine("return check");
                Console.WriteLine("\n" + check);

            }

            public void createReturnd()
            {
                Console.WriteLine("Созан класс createReturnd");
                this.returnd = new returnd();
            }

            public void checkReturnd(string check, string GameName)
            {
                Console.WriteLine("вызов метода checkReturnd(check, GameName) ");
                this.gameForClient = findGame(GameName);
                Console.WriteLine("вызов метода checkReturnd(check, GameName) ");
                this.resultReturnd = this.returnd.checkReturnd(check, gameForClient);
            }

            public void rsturmMoany(int idTicket, string check)
            {
                if (this.resultReturnd)
                {
                    Console.WriteLine("вызов метода returnTicket()");
                    this.returnd.returnTicket(idTicket, this.gameForClient);
                    Console.WriteLine("вызов метода returnMone");
                    this.payment.returnMoney(check);
                }
                else
                    Console.WriteLine("в возрате отказано");
            }




        }
  
        internal class Game
        {
            string name;
            int ticketCount;
            int FreeTicketCount;
            OneTimeTicket[] tickets;
            Stadium stadium;

            public Game(string name, int ticketCount)
            {
                this.name = name;
                this.ticketCount = ticketCount;
                this.FreeTicketCount = ticketCount;
                this.tickets = new OneTimeTicket[ticketCount];
                for (int i = 0; i < ticketCount; i++)
                {
                    this.tickets[i] = new OneTimeTicket(i + 1);
                }
                this.stadium = new Stadium("Газпром-арена");

            }

            public string getInformation()
            {
                Console.WriteLine("вызов метода getInformation()");
                string nameStadium = stadium.getInfStadium();
                Console.WriteLine("return stadium name");
                string select = "Stadium: " + nameStadium + "\n" +
                                "Name Game: " + name + "\n" +
                                "мест: " + ticketCount + "\n" +
                                "свободных мест: " + FreeTicketCount;
                return select;
            }

            public string getName()
            {
                return this.name;
            }

            public int[] putInSale(int place)
            {
                Console.WriteLine("вызов метода putInSale(place)");
                OneTimeTicket ticket = findTicet(place);

                if ((ticket == null) || (ticket.getFree() == false))
                    return null;

                int[] infTicket = new int[2];

                infTicket[0] = ticket.getPrice();
                infTicket[1] = ticket.getIdTicket();

                Console.WriteLine("return price, id");
                return infTicket;
            }

            public OneTimeTicket findTicet(int place)
            {
                Console.WriteLine("вызов метода findTicet(place)");
                for (int i = 0; i < tickets.Length; i++)
                    if (tickets[i].getIdTicket(" - выполняется при поиске билета") == place)
                    {
                        Console.WriteLine("return Tickets");
                        return tickets[i];
                    }
                return null;
            }

            public void returnTicket(int idTicket)
            {
                Console.WriteLine("вызов метода returnByld()");
                this.tickets[idTicket].returnByld();
            }

        }
    


        internal class OneTimeTicket : Ticket
        {
            int id;
            int price = 100;
            bool free = true;

            public OneTimeTicket(int id)
            {
                this.id = id;
            }

            public int getPrice()
            {
                Console.WriteLine("вызов метода getPrice()");
                this.free = false;
                return this.price;
            }

            public int getIdTicket(string massage = " ")
            {
                Console.WriteLine("вызов метода getIdTicket()" + massage);
                return this.id;
            }

            public bool getFree()
            {
                return this.free;
            }

            public void returnByld()
            {

                this.free = false;
            }
        }
    

  
        internal class Payment
        {
            string check = "успешная покупка";
            int price;
            bool successfulPayment = false;
            List<int> idTicket;

            public Payment(int price, List<int> idTicket)
            {
                this.price = price;
                this.idTicket = idTicket;
            }

            public string pay(int money)
            {
                Console.WriteLine("вызов метода pay(money)");
                successfulPayment = money >= price ? true : false;

                if (successfulPayment)
                {
                    Console.WriteLine("return check");
                    return check;
                }
                else
                {
                    return "error";
                }
            }

            public int returnMoney(string check)
            {
                Console.WriteLine("return moany");
                return this.price;
            }
        }



        class returnd
        {
            bool result = false;
            public bool checkReturnd(string check, Game game)
            {
                game.getInformation();
                this.result = true;
                return getResult();
            }

            public bool getResult()
            {
                return this.result;
            }

            public void returnTicket(int idTicket, Game game)
            {
                Console.WriteLine("вызов метода returnTicke()");
                game.returnTicket(idTicket);
            }
        }
    


        internal class Sale
        {
            List<int> idList = new List<int>();
            int price;
            Game game;
            int[] infTicket = new int[2];

            public Sale(Game game)
            {
                this.game = game;
            }

            public void addTicketToSale(int place)
            {
                Console.WriteLine("addTicketToSale(place)");
                infTicket = this.game.putInSale(place);
                this.price += infTicket[0];
                this.idList.Add(infTicket[1]);
            }

            public int getPrice()
            {
                Console.WriteLine("вызов метода getPrice()");
                return this.price;
            }

            public List<int> getListTicket()
            {
                Console.WriteLine("вызов метода getListTicket()");
                return this.idList;
            }
        }
    

        internal class SesonTicket : Ticket
        {
            public bool dateOfExpired;
        }



        internal class Stadium
        {
            string name;

            public Stadium(string name)
            {
                this.name = name;
            }

            public string getInfStadium()
            {
                Console.WriteLine("вызов метода getInfStadium()");
                return name;
            }


        }
    


    
        internal class Ticket
        {


        }
    





}
